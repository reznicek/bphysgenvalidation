/**
 * Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
 *
 * Simple macro to run the validation, using BPhysGenValidation class.
 * Goal is to validate truth DAOD contents, speciazed on B-physics decays.
 * Loosely based on PhysicsAnalysis/DerivationFramework/DerivationFrameworkAnalysisTests.
 */

#include "BPhysGenValidation.h"

#include "TAttMarker.h"
#include "TCanvas.h"
#include "TDirectory.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TList.h"
#include "TObject.h"
#include "TString.h"
#include "TStyle.h"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <cmath>
#include <algorithm>
#include <cstring>
#include <iostream>



/********************************//**
 * Main routine of the validation.
 ***********************************/

int main(int argc, char **argv) {

  ///
  /// The application's name:
  ///
  const char* APP_NAME = argv[0];

  ///
  /// Get and parse the command-line arguments
  ///
  po::options_description desc("Running a simple truth analysis");
  std::string outputName, inputName;
  desc.add_options()
    ("help,h", "print usage and exit")
    ("output,o",po::value<std::string>(&outputName), "Output file name")
    ("input,i", po::value<std::string>(&inputName) , "Input file name")
    ("nevents", po::value<int>()->default_value(-1), "number of events to run on (set to -1 to ignore it)")
    ;

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
  po::notify(vm);

  if ( vm.count("help") ) {
    std::cout << desc << std::endl;
    return 1;
  }

  long long nevents = -1;
  if ( vm.count("nevents") ) {
    nevents = vm["nevents"].as<int>();
  }

  ///
  /// Construct the validator
  ///
  BPhysGenValidation b(APP_NAME, inputName.c_str(), outputName.c_str(), nevents, "TruthParticles", true);

  ///
  /// Loop over the events and fill basic histograms
  ///
  TH1F h_decays ("h_decays" , "B-decays in the events ; ; Candidates", 1, 0, 1);
  TH2F h_muon_pT("h_muon_pT", "2D histogram of the muons p_{T} ; p_{T}(#mu^{+}) [GeV] ; p_{T}(#mu^{-}) [GeV]", 40, 0, 20, 40, 0, 20);
  TH1F h_Btau   ("h_Btau"   , "Histogram of b-hadrons proper decay time ; #tau(B) [ps] ; Entries", 100, 0, 20);

  for ( int i=0; i<b.getN(); i++ ) {

    b.readEvent(i);
    h_decays.Fill("All Events", 1 + 0.001/b.getN()); // small correction to make sure "All Events" is the first bin
    xAOD_CTP *c = b.getContainer("TruthParticles");

    for ( const auto *p : *c ) {
      if ( p->isBottomHadron() && !b.pdtName(p).Contains("*") ) {

        TString decay = b.getDecayString(p);
        h_decays.Fill(decay, 1);
        xAOD_TP *p_muon_pos = b.findDaughter(p, "mu+");
        xAOD_TP *p_muon_neg = b.findDaughter(p, "mu-");
        if ( p_muon_pos && p_muon_neg ) {
          h_muon_pT.Fill(p_muon_pos->pt()/1000, p_muon_neg->pt()/1000);
        }
        h_Btau.Fill(b.getTau(p));
      }
    }
  }

  ///
  /// Sort decays by most efficient bins, cut draw range to avoid unimportat backgrounds
  ///
  h_decays.LabelsDeflate("X");
  h_decays.LabelsOption(">");
  int h_decays_lastbin = 1;
  for ( ; h_decays_lastbin <= h_decays.GetNbinsX(); h_decays_lastbin++ ) {
    if ( h_decays.GetBinContent(h_decays_lastbin) < 0.2*h_decays.GetBinContent(h_decays.GetXaxis()->FindBin("All Events")) ) break;
  }

  ///
  /// Draw the histograms as well and store as PDF and CXX file
  ///
  gStyle->SetOptStat(0);
  TCanvas c("c", "Summary of all the plots", 1000, 250 * 2);
  c.Divide(1, 2);
  c.cd(1)->SetBottomMargin(0.5); h_decays.DrawCopy("hist")->GetXaxis()->SetRangeUser(0, std::max(2*h_decays_lastbin, 20));
  c.cd(2)->Divide(4, 1);
  c.cd(2)->cd(1); h_muon_pT.DrawCopy()->SetMarkerStyle(kFullDotSmall);
  c.cd(2)->cd(2); h_Btau   .DrawCopy();
  c.SaveAs(TS(outputName).ReplaceAll(".root", ".pdf"));
  c.SaveAs(TS(outputName).ReplaceAll(".root", ".cxx"));

  ///
  /// Store the histograms into the output file
  ///
  TList *memDir = gDirectory->GetList();
  TFile *f = TFile::Open(outputName.c_str(), "RECREATE");
  for ( int i=0; i<memDir->GetSize(); i++ ) if ( TS(memDir->At(i)->GetName()).Contains("h_") ) {
    printf("Writing output histogram: %s\n", memDir->At(i)->GetName());
    memDir->At(i)->Write();
  }
  f->Close();
}
