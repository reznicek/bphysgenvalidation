/*
  * Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
  *
  * The common parts of the tool: initialization, output, helper functions
*/

#include "BPhysGenValidation.h"

#include "AsgTools/AnaToolHandle.h"

#include "HepPDT/defs.h"
#include "HepPDT/TableBuilder.hh"
#include "HepPDT/ParticleData.hh"
#include "HepPDT/ParticleDataTable.hh"

#include "TObjArray.h"
#include "TObjString.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <utility>



/*********//**
 * Cleanup.
 ************/

BPhysGenValidation::~BPhysGenValidation() {

  delete m_event;
  delete m_pdt;
  delete m_inFile;
  delete m_outFile;
  delete m_t;
}



/**********************************//**
 * Constructor initializing the job.
 *************************************/

BPhysGenValidation::BPhysGenValidation(TS appName, TS inputName, TS outputName, long long nEvents, TS containers, bool debug) {

  m_appName = appName;
  OpenFile(inputName, outputName, nEvents, containers, debug);
}

int BPhysGenValidation::OpenFile(TS inputName, TS outputName, long long nEvents, TS containers, bool debug) {

  ///
  /// Initialize basic variables of the job
  ///
  m_inputName  = inputName;
  m_outputName = outputName;
  m_t          = nullptr;
  m_outFile    = nullptr;
  m_nEvents    = nEvents;
  m_debug      = debug;
  m_containers.clear();
  TObjArray *tc = containers.Tokenize(" ");
  for ( int i=0; i<tc->GetEntries(); i++ ) {
    m_containers.push_back(((TObjString*)(tc->At(i)))->String());
    m_c[m_containers[i]] = nullptr;
  }

  printf("Reading from %s and writing to %s\n", m_inputName.Data(), m_outputName.Data());
  printf("Will analyze %ld containers:\n", m_containers.size());
  for ( size_t i=0; i<m_containers.size(); i++ ) printf("  %s\n", m_containers[i].Data());

  ///
  /// Make sure things know we are not in StatusCode land
  ///
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE(int);

  ///
  /// Setup for reading -- if this fails, we have major problems
  ///
#ifdef XAOD_STANDALONE
  if ( ! xAOD::Init().isSuccess() ) {
    throw std::runtime_error("Cannot initialise xAOD access !");
  }
  ANA_MSG_INFO("Using xAOD access");
#else
  SmartIF<IAppMgrUI> m_app = POOL::Init();
  ANA_MSG_INFO("Using POOL access");
#endif

  ///
  /// Input data
  ///
  m_inFile = TFile::Open(m_inputName.Data(), "READ");
  ANA_CHECK(m_inFile);
#ifdef XAOD_STANDALONE
  m_event = new xAOD::TEvent(xAOD::TEvent::kClassAccess);
#else
  m_event = new POOL::TEvent(POOL::TEvent::kClassAccess);
#endif
  ANA_CHECK(m_event->readFrom(m_inFile));

  printf("Found %lld entries", static_cast<long long>(m_event->getEntries()));
  if ( m_nEvents == -1 ) {
    printf(", will analyze all\n");
    m_nEvents = static_cast<long long>(m_event->getEntries());
  } else {
    printf(", will analyze %lld\n", m_nEvents);
  }

  ///
  /// Initialize the Particle Data Table
  ///
  std::vector<TS> pdtFiles = { "evt.pdl", "DECAY.DEC", "ATLAS.pdl" };
  m_pdt = new HepPDT::ParticleDataTable("PDT Table");
  {
    HepPDT::TableBuilder pdtBuilder(*m_pdt);
    for ( size_t i=0; i<pdtFiles.size(); i++ ) {
      std::ifstream f(pdtFiles[i]);
      if( !addEvtGenParticles(f, pdtBuilder) ) { printf("HepPDT: Error reading %s file\n", pdtFiles[i].Data()); }
    }
  }

  if ( m_debug ) {
    ///
    /// Show the particles in the first event
    ///
    printf("Reading event 0 from the data.\n");
    readEvent(0);
    printf("Particles in the first event of the input data:\n");
    for ( const auto *p : *m_c["TruthParticles"] ) {
      printf("  %10d %10s  #children = %ld, total #children = %d, total #mothers = %d\n",
             p->pdgId(), pdtName(p).Data(), ((p->hasDecayVtx())?p->decayVtx()->nOutgoingParticles():0), countChildren(p), countParents(p));
    }
    printf("Decays of b-hadrons in the first event of the input data:\n");
    for ( const auto *p : *m_c["TruthParticles"] ) {
      if ( p->isBottomHadron() ) printf("  %s\n", getDecayString(p, FSR::Include).Data());
    }
    /*printf("Finding B+ -> J/psi(mu+mu-) K+ sub-particles:\n");
    for ( const auto *p : *m_c["TruthParticles"] ) {
      if ( p->isBottomHadron() ) {
        if ( getDecayString(p) == "B+(J/psi_a(mu+,mu-),K+)" ) {
          printf("  B+:             %p\n", (void*)p);
          printf("  B+->K+:         %p\n", (void*)(p->child(1)));
          printf("  B+->J/psi:      %p\n", (void*)(p->child(0)));
          printf("  B+->J/psi->mu+: %p\n", (void*)(p->child(0)->child(0)));
          printf("  B+->J/psi->mu-: %p\n", (void*)(p->child(0)->child(1)));
          printf("  J/psi: %p\n", (void*)findDaughter(p, "J/psi_a"));
          printf("  K+: %p\n", (void*)findDaughter(p, "K+"));
          printf("  K+: %p\n", (void*)findDaughter(p, "->K+"));
          printf("  K+: %p\n", (void*)findDaughter(p, "B+->K+"));
          printf("  K+: %p\n", (void*)findDaughter(p, "Bs->K+"));
          printf("  K+: %p\n", (void*)findDaughter(p, "J/psi_a->K+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "->J/psi_a->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->J/psi_a->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->999443#2->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->999443->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->999443->mu+#1"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->999443->mu+#2"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "B+->43->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "Bs->J/psi_a->mu+"));
          printf("  mu+: %p\n", (void*)findDaughter(p, "J/psi_a->mu+"));
        }
      }
    }*/
  }

  return 0;
}



/******************************************//**
 * Macro to read containers for given event.
 *********************************************/

bool BPhysGenValidation::readEvent(long long i) {

  m_event->getEntry(i);

  for ( auto &c : m_c ) {
    if ( !m_event->retrieve(c.second, c.first.Data()).isSuccess() ) {
      Error(m_appName, "Could not load event container %s from the file!", c.first.Data());
      throw std::runtime_error("Container retrieval failed");
    } else {
      if ( m_debug ) printf("Successfully read container %s\n", c.first.Data());
    }
  }

  return true;
}



/*******************************************************************************************************//**
 * Helper for detecting FSR in decay of the particle.
 * FSR pseudo-definition: photon daughters, but there must be also another non-photon daughter alongside,
 *                        exclude decays of excited B-hadrons with "*" in the name,
 *                        exclude pure photon decays.
 * Caveat: Supresses also photons from real decays of other excited particles like X(YYYY) resonances.
 *         Devoted to detection of FSR in B-decays that do not include photons under non-FSR conditions.
 **********************************************************************************************************/

bool BPhysGenValidation::hasFSRinDecay(xAOD_TP *p) {

  if ( !p ) return false;
  if ( pdtName(p).Contains("*") && p->isBottomHadron() ) return false;

  int photons = 0;
  int others  = 0;
  for ( size_t n=0; n<p->nChildren(); n++ ) if ( p->child(n) ) {
    if ( p->child(n)->pdgId() == 22 ) photons++;
    else                              others++;
  }

  return (photons > 0 && others > 0);
}



/***************************************//**
 * Get tota 4-vector of set of particles.
 ******************************************/

TLorentzVector BPhysGenValidation::getM(std::vector<xAOD_TP*> p) {

  TLorentzVector mother(0., 0., 0., 0.);
  for ( size_t n=0; n<p.size(); n++ ) if ( p[n] ) {
    mother += p[n]->p4();
  }
  return mother;
}



/**************************************//**
 * Get total charge of set of particles.
 *****************************************/

double BPhysGenValidation::getQ(std::vector<xAOD_TP*> p) {

  double charge = 0;
  for ( size_t n=0; n<p.size(); n++ ) if ( p[n] ) {
    charge += p[n]->charge();
  }
  return charge;
}



/***********************************************************//**
 * Helper for counting all children, possibly ignore photons.
 **************************************************************/

int BPhysGenValidation::countChildren(xAOD_TP *p, bool ignoreFSR) {

  if ( !p ) return 0;

  bool foundFSR = false;
  if ( ignoreFSR ) foundFSR = hasFSRinDecay(p);

  int children = 0;
  for ( size_t n=0; n<p->nChildren(); n++ ) if ( p->child(n) ) {
    if ( ignoreFSR && foundFSR && p->child(n)->pdgId() == 22 ) continue;
    children++;
    children += countChildren(p->child(n));
  }

  return children;
}



/*********************************//**
 * Helper for counting all parents.
 ************************************/

int BPhysGenValidation::countParents(xAOD_TP *p, bool ignorePartons) {

  if (!p) return 0;

  int parents = 0;
  for ( size_t n=0; n<p->nParents(); n++ ) if ( p->parent(n) ) {
    ///
    /// Skip special particles, but still browse their parents, that can be normal particles, especially in case of Pythia di-quarks
    /// https://github.com/mortenpi/pythia8/blob/master/xmldoc/ParticleData.xml
    ///
    if ( ! (ignorePartons && (p->parent(n)->isParton() || // skip quarks and gluons
                              isSpecial(p->parent(n))  || // skip generator-helpers, random-flavour qqbar, Pomeron and Reggeon
                              isDiQuark(p->parent(n))  || // skip helper di-quarks from Pythia
                             (p->parent(n)->pdgId() == 2212 && p->parent(n)->nParents() == 0) ) ) // skip colliding protons
       ) parents++;
    parents += countParents(p->parent(n));
  }

  return parents;
}



/***************************************************************//**
 * Sort children by their (abs) of PDG-id, negative PDG-id first.
 ******************************************************************/

std::vector<xAOD_TP*> BPhysGenValidation::sortChildrenByPdgId(xAOD_TP *p) {

  std::vector<std::pair<double, xAOD_TP*> > pdgId;
  for ( size_t n=0; n<p->nChildren(); n++ ) {
    if ( p->child(n) ) {
      pdgId.push_back(std::make_pair(abs(p->child(n)->pdgId()) + 0.5*(p->child(n)->pdgId()>0), p->child(n)));
    } else {
      pdgId.push_back(std::make_pair(n*1e12, p->child(n))); // put null pointers to the back
    }
  }

  sort(pdgId.begin(), pdgId.end());
  std::vector<xAOD_TP*> sorted;
  for ( size_t n=0; n<p->nChildren(); n++ ) sorted.push_back(pdgId[n].second);
  return sorted;
}



/*********************************************//**
 * Sort children by their names alphabetically.
 ************************************************/

std::vector<xAOD_TP*> BPhysGenValidation::sortChildrenByName(xAOD_TP *p) {

  std::vector<std::pair<TS, xAOD_TP*> > pdgId;
  for ( size_t n=0; n<p->nChildren(); n++ ) {
    if ( p->child(n) && pdtName(p->child(n)) != "UNKNOWN" && pdtName(p->child(n)) != "" ) {
      pdgId.push_back(std::make_pair(pdtName(p->child(n)), p->child(n))); // TODO?: Use tolower to have case-insensitive sorting. But what about possible duplicates?
    } else {
      pdgId.push_back(std::make_pair("UNKNOWN:"+TS::Itoa(n,10), p->child(n)));
    }
  }

  sort(pdgId.begin(), pdgId.end());
  std::vector<xAOD_TP*> sorted;
  for ( size_t n=0; n<p->nChildren(); n++ ) sorted.push_back(pdgId[n].second);
  return sorted;
}



/******************************************************************************//**
 * Construction of a decay string, e.g. Bs(J/psi(mu+,mu-),phi(K+,K-)).
 * Particles in a generation are sorted according to their names alphabetically.
 * Possibly emphasise selected daugher partices using ROOT #bf{} style.
 * Credits: Eliška Korcová (student projekt at IPNP MFF CUNI).
 *********************************************************************************/

TS BPhysGenValidation::getDecayString(xAOD_TP *p, FSR photon, std::vector<xAOD_TP *> daughters, bool topParticle) {

  if ( !p ) return "";

  bool foundFSR = false;
  if ( photon != FSR::Include && photon != FSR::Ignore ) foundFSR = hasFSRinDecay(p);

  TS decay    = "";
  TS subDecay = "";
  std::vector<xAOD_TP*> p_child = sortChildrenByName(p);
  for ( size_t n=0; n<p_child.size(); n++ ) if ( p_child[n] ) {
    if ( photon != FSR::Include && photon != FSR::Ignore && foundFSR && p_child[n]->pdgId() == 22 ) {
      photon = FSR::Found;
    } else if ( photon != FSR::Ignore || p_child[n]->pdgId() != 22 ) {
      if ( decay != "" ) decay += ",";
      subDecay = getDecayString(p_child[n], photon, daughters, false);
      if ( subDecay.Contains("_FSR") ) {
        photon = FSR::Found;
        decay += subDecay(0, subDecay.Index("_FSR"));
      } else {
        decay += subDecay;
      }
    }
  }

  TS mother = pdtName(p);
  if ( std::find(daughters.begin(), daughters.end(), p) != daughters.end() ) mother = "#bf{"+mother+"}";

  if ( decay != "" ) decay = mother + "(" + decay + ")";
  else               decay = mother;

  if ( photon == FSR::Found                 ) decay = decay + "_FSR";
  if ( photon == FSR::Ignore && topParticle ) decay = decay + "_gammaIgnored";

  return decay;
}




/************************************************************************************//**
 * Routine to find daughter in the decay tree, either specified by name, or by PDG-id,
 * use e.g. "Bs->J/psi#2->mu-" to find mu- of the 2nd J/psi from a Bs decay,
 *            "->J/psi#2->mu-" would do the same,
 * use e.g. "mu-" to find first mu- anywhere in the Bs decay tree.
 ***************************************************************************************/


xAOD_TP* BPhysGenValidation::findDaughter(xAOD_TP *p, TS decayPath, bool topParticle, bool singleParticle) {

  if ( !p ) return nullptr;

  int              topPdgId = 0;
  std::vector<int> pdgId;
  std::vector<int> position;

  ///
  /// Decode the decay chain to the searched particle
  ///
  decayPath.ReplaceAll(" " , "" );
  decayPath.ReplaceAll(")" , "");
  decayPath.ReplaceAll("(" , ">");
  decayPath.ReplaceAll("," , ">");
  decayPath.ReplaceAll("->", ">");
  if ( decayPath[0] == '>' ) decayPath = "0" + decayPath;

  TObjArray *particles = decayPath.Tokenize(">");
  for ( int i=0; i<particles->GetEntries(); i++ ) {
    TS  particle_string   = ((TObjString*)(particles->At(i)))->String();
    int particle_position = 1;
    if ( particle_string.Contains("#") ) {
      TS particle_tmp   = particle_string(0, particle_string.Index("#"));
      TS position_tmp   = particle_string(   particle_string.Index("#")+1, particle_string.Length());
      particle_string   = particle_tmp;
      particle_position = position_tmp.Atoi();
      if ( !position_tmp.IsDigit() && particle_position < 2 ) {
        printf("ERROR in findDaughter: bad position %s of particle %s\n", position_tmp.Data(), particle_tmp.Data());
        return nullptr;
      }
    }
    if ( particle_string.IsFloat() ) {
      pdgId.push_back(particle_string.Atoi());
    } else {
      int pdgId_tmp = pdtPid(particle_string);
      if ( pdgId_tmp == 0 ) {
        printf("ERROR in findDaughter: unknown particle %s\n", particle_string.Data());
        return nullptr;
      }
      pdgId.push_back(pdgId_tmp);
    }
    position.push_back(particle_position);
  }

  xAOD_TP *daughter = nullptr;

  ///
  /// Special case of single particle, not requiring specific position in the decay tree
  ///
  if ( singleParticle || (topParticle && pdgId.size() == 1) ) {
    if ( topParticle ) m_findDaughter_position = 0; // position anywhere in the decay tree
    for ( size_t n=0; n<p->nChildren(); n++ ) if ( p->child(n) ) {
      if ( p->child(n)->pdgId() == pdgId[0] ) {
        m_findDaughter_position++;
        if ( position[0] == m_findDaughter_position ) return p->child(n);
      }
      daughter = findDaughter(p->child(n), decayPath, false, true);
      if ( daughter ) return daughter;
    }
  } else {
    ///
    /// In case of the decay chain, the first particle is the top-mother "p"
    ///
    if ( topParticle ) {
      topPdgId = pdgId[0];
      pdgId   .erase(pdgId   .begin());
      position.erase(position.begin());
      decayPath = decayPath(decayPath.Index(">")+1, decayPath.Length());
    }
    ///
    /// Case when top-mother does not match
    ///
    if ( topParticle && topPdgId != p->pdgId() && topPdgId != 0 ) {
      return nullptr;
    } else {
    ///
    /// General case respecting decay tree
    ///
      int local_position = 0; // local position at this level of the decay tree
      for ( size_t n=0; n<p->nChildren(); n++ ) if ( p->child(n) ) {
        if ( p->child(n)->pdgId() == pdgId[0] ) {
          local_position++;
          if ( position[0] == local_position ) {
            if ( pdgId.size() == 1 ) {
              return p->child(n);
            } else {
              daughter = findDaughter(p->child(n), decayPath(decayPath.Index(">")+1, decayPath.Length()), false, false);
              if ( daughter ) return daughter;
            }
          }
        }
      }
    }
  }

  return nullptr;
}



/*****************************************************************************//**
 * Convert particle name or pdg-ID into code-readable name (for TTree::Branch).
 ********************************************************************************/

TS BPhysGenValidation::name2code(TS name, bool withUnderscores) {

  if ( withUnderscores ) {
    name.ReplaceAll("anti-", "anti_");
    name.ReplaceAll(")0"   , "_0");
    name.ReplaceAll("("    , "");
    name.ReplaceAll(")"    , "");
    name.ReplaceAll("/"    , "");
    name.ReplaceAll("*"    , "st");
    name.ReplaceAll("'"    , "pr");
    name.ReplaceAll("+"    , "_pos");
    name.ReplaceAll("-"    , "_neg");
  } else {
    name.ReplaceAll("anti-", "anti");
    name.ReplaceAll(")0"   , "zero");
    name.ReplaceAll("("    , "");
    name.ReplaceAll(")"    , "");
    name.ReplaceAll("/"    , "");
    name.ReplaceAll("*"    , "st");
    name.ReplaceAll("'"    , "pr");
    name.ReplaceAll("+"    , "Pos");
    name.ReplaceAll("-"    , "Neg");
    name.ReplaceAll("_"    , "");
  }

  return name;
}

TS BPhysGenValidation::pdgid2code(int pdgId) {

  if ( pdgId < 0 ) return Form("m%d", pdgId);
  else             return Form(" %d", pdgId);
}



/*******************************************************************************//**
 * Routine to find all daughters and return vector of their names in two formats:
 * ala: Bs_Jpsi_muPlus, Bs_Phi_Kplus etc.
 * ala: 531_443_m13 etc.
 * all for the purpose of ntuple-creation
 * TODO: Missing case when there are same particles at the same level
 **********************************************************************************/
/*
std::vector<std::pair<TS, TS> > BPhysGenValidation::getChildrenTreeNames(xAOD_TP *p, bool includeMothers, bool ignoreFSR, TString mothersName, TString mothersPdgId) {

  std::vector<std::pair<TS, TS> > childTree;
  if ( !p ) return childTree;

  bool foundFSR = false;
  if ( ignoreFSR ) foundFSR = hasFSRinDecay(p);

  ///
  /// Insert the current particle and update mothers-string for its children
  ///
  if ( includeMothers && mothersName  != "" ) mothersName  = mothersName  + "_";
  if ( includeMothers && mothersPdgId != "" ) mothersPdgId = mothersPdgId + "_";
  childTree.push_back(std::make_pair(mothersName + name2code(pdtName(p)), mothersPdgId + pdgid2code(p->pdgId())));
  if ( includeMothers ) mothersName  = mothersName  + name2code(pdtName(p));
  if ( includeMothers ) mothersPdgId = mothersPdgId + pdgid2code(p->pdgId());

  ///
  /// Insert its children as well
  ///
  for ( size_t n=0; n<p->nChildren(); n++ ) if ( p->child(n) ) {
    if ( ignoreFSR && foundFSR && p->child(n)->pdgId() == 22 ) continue;
    subChildTree = getChildrenTreeNames(p->child(n), includeMothers, ignoreFSR, mothersName, mothersPdgId);
    childTree.insert(childTree.end(), subChildTree.begin(), subChildTree.end());
  }

  return childTree;
}
*/
