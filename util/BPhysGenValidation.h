/**
 * Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
 *
 * Simple class for working with truth DAODs.
 * Goal is to validate truth DAOD contents, speciazed on B-physics decays.
 * Loosely based on PhysicsAnalysis/DerivationFramework/DerivationFrameworkAnalysisTests.
 */

#ifndef BPHYSGENVALIDATION_H
#define BPHYSGENVALIDATION_H

#ifdef XAOD_STANDALONE
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#else
#include "POOLRootAccess/TEvent.h"
#include "GaudiKernel/SmartIF.h"
#endif

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "HepPDT/ParticleData.hh"
#include "HepPDT/ParticleDataTable.hh"

#include "TFile.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TTree.h"

#include <vector>
#include <map>

///
/// Shortcuts
///
#define TS TString
#define xAOD_CTP const xAOD::TruthParticleContainer
#define xAOD_TP const xAOD::TruthParticle

///
/// Enum to clasify FSR detection
///
enum class FSR { Include, Ignore, Found, NotFound };

/**
 * Class for the TRUTH0 analysis dedicated to B-decays
 */

class BPhysGenValidation {

  public:

    ///
    /// Default constructor
    ///
    BPhysGenValidation(TS appName = "BPhysGenValidation", TS inputName = "DAOD.TRUTH0.root", TS outputName = "bphys.root", long long nEvents = -1,
                       TS containers = "TruthParticles TruthElectrons TruthMuons TruthPhotons TruthTaus TruthNeutrinos TruthBSM TruthBottom TruthTop TruthBoson",
                       bool debug = false);
    ~BPhysGenValidation();

    ///
    /// Debugging on/off
    ///
    inline void setDebug(bool debug = true) { m_debug = debug; };
    inline bool getDebug() { return m_debug; };

    ///
    /// Recursivelly count total number of children or parents
    ///
    int countChildren (xAOD_TP *p, bool ignoreFSR     = false);
    int countParents  (xAOD_TP *p, bool ignorePartons = true );

    ///
    /// Construct string of the decay tree (i.e. recursively to all daugther levels)
    ///
    TS  getDecayString(xAOD_TP *p, FSR photon = FSR::NotFound, std::vector<xAOD_TP *> daughters = {}, bool topParticle = true);

    ///
    /// Find specific daughter in the decay tree of mother particle
    ///
    xAOD_TP* findDaughter(xAOD_TP *p, int pdgId) { return findDaughter(p, TS::Itoa(pdgId, 10)); };
    xAOD_TP* findDaughter(xAOD_TP *p, TS decayPath, bool topParticle = true, bool singleParticle = false);

    ///
    /// Read all configured containers of given event
    ///
    bool readEvent(long long i);

    inline TS        getAppName()    { return m_appName; };
    inline TS        getInputName()  { return m_inputName; };
    inline long long getN()          { return m_nEvents; };

    inline TTree*    getOutputTree() { return m_t; };
    inline TS        getOutputName() { return m_outputName; };
    inline TFile*    getOutputFile() { return m_outFile; };

    inline xAOD_CTP* getContainer(TS containerName) { return m_c.count(containerName) ? m_c[containerName] : nullptr; };

    ///
    /// Helper routines, still useful also for the class user
    ///
    inline int pdtPid (TS name)    { if ( !m_pdt ) return 0;
                                     HepPDT::ParticleData *pdt = m_pdt->particle(name.Data()); return ((pdt)?pdt->pid():0); };
    inline TS  pdtName(int pdgId)  { if ( !m_pdt ) return "UNKNOWN";
                                     HepPDT::ParticleData *pdt = m_pdt->particle(HepPDT::ParticleID(pdgId)); if ( !pdt || pdt->name().empty() ) return "pid"+TS::Itoa(pdgId, 10).ReplaceAll("-", "n");
                                     return pdt->name().c_str(); };
    inline TS  pdtName(xAOD_TP *p) { return ((p)?pdtName(p->pdgId()):"UNKNOWN"); };

    inline bool isDiQuark(xAOD_TP *p) { return (                      p->pdgId() == 1103 ||
                                                p->pdgId() == 2101 || p->pdgId() == 2103 ||                       p->pdgId() == 2203 ||
                                                p->pdgId() == 3101 || p->pdgId() == 3103 || p->pdgId() == 3201 || p->pdgId() == 3203 ||                       p->pdgId() == 3303 ||
                                                p->pdgId() == 4101 || p->pdgId() == 4103 || p->pdgId() == 4201 || p->pdgId() == 4203 || p->pdgId() == 4301 || p->pdgId() == 4303 ||                       p->pdgId() == 4403 ||
                                                p->pdgId() == 5101 || p->pdgId() == 5103 || p->pdgId() == 5201 || p->pdgId() == 5203 || p->pdgId() == 5301 || p->pdgId() == 5303 || p->pdgId() == 5401 || p->pdgId() == 5403 || p->pdgId() == 5503); };
    inline bool isSpecial(xAOD_TP *p) { return (p->isGenSpecific() || p->pdgId() == 90  || p->pdgId() == 92 ||
                                                p->pdgId() == 81   || p->pdgId() == 82  || p->pdgId() == 83 ||
                                                p->pdgId() == 990  || p->pdgId() == 110); };

    ///
    /// Check if there is a possible final-state radiation photon in the decay tree
    ///
    bool hasFSRinDecay(xAOD_TP *p);

    ///
    /// Get total 4-vectors or total charge of set of particles
    ///
    TLorentzVector getM(std::vector<xAOD_TP*> p);
    double         getQ(std::vector<xAOD_TP*> p);

    ///
    /// Get Lxy[mm] or (pseudo) proper decay time [ps] or c*tau[mm] of a particle
    /// (pseudo proper decay time assumes primary vertex at (0,0,0))
    ///
    inline double  getLxy       (xAOD_TP* p) { return (!p || !(p->decayVtx()) || !(p->prodVtx())) ? 0. : (p->decayVtx()->v4() - p->prodVtx()->v4()).Perp(); };
    inline double  getTau       (xAOD_TP* p) { return (!p || !(p->decayVtx()) || !(p->prodVtx())) ? 0. : (p->decayVtx()->t() - p->prodVtx()->t())/(p->e()/p->m()) * 1e9/299792458.; };
    inline double  getcTau      (xAOD_TP* p) { return (!p || !(p->decayVtx()) || !(p->prodVtx())) ? 0. : (p->decayVtx()->t() - p->prodVtx()->t())/(p->e()/p->m()); };
    inline double  getTauPseudo (xAOD_TP* p) { return (!p || !(p->decayVtx()) || !(p->prodVtx())) ? 0. : p->decayVtx()->t()/(p->e()/p->m()) * 1e9/299792458.; };
    inline double  getcTauPseudo(xAOD_TP* p) { return (!p || !(p->decayVtx()) || !(p->prodVtx())) ? 0. : p->decayVtx()->t()/(p->e()/p->m()); };

    ///
    /// Translate particle PDT name or PDG-id to code-friendly string
    ///
    TS name2code(TS  name, bool withUnderscore = false);
    TS pdgid2code(int pdgId);

    ///
    /// Return list of children sorted by their PDG-id or by their PDT name
    ///
    std::vector<xAOD_TP*> sortChildrenByPdgId(xAOD_TP *p);
    std::vector<xAOD_TP*> sortChildrenByName (xAOD_TP *p);

    /**
     * TODO:?
     *
     * std::vector<std::pair<TS, TS> > getChildrenTreeNames(xAOD_TP *p, bool includeMothers = true, bool ignoreFSR = false, TS mothersName = "", TS mothersPdgId = "");
     * Routine to find first common mother of children
     * Routine to create TTree branches and associate temporary variables with them (getChildrenNames: name, variables, sub-branches, ...)
     */

  private:

    ///
    /// Real constructor (because of ANA_CHECK macro needs to return a value)
    ///
    int OpenFile(TS inputName, TS outputName, long long nEvents, TS containers, bool debug);

    ///
    /// Application name for messaging
    ///
    TS m_appName;

    ///
    /// Input
    ///
    TS   m_inputName;
    long long m_nEvents;
    std::vector<TS> m_containers;

    /**
     * Output TTree and file and its name
     * TODO: other possible containers of non-TruthParticleContainer type
     */
    TTree *m_t;
    TS     m_outputName;
    TFile *m_outFile;

    ///
    /// Containers
    ///
#ifdef XAOD_STANDALONE
    xAOD::TEvent *m_event;
#else
    POOL::TEvent *m_event;
#endif
    std::map<TS, xAOD_CTP*> m_c;
    /**
     * TODO: other possible containers of non-TruthParticleContainer type
     */

    ///
    /// Helper variables
    ///
    HepPDT::ParticleDataTable *m_pdt;
    SmartIF<IAppMgrUI> m_app;
    TFile *m_inFile;
    bool m_debug;
    int m_findDaughter_position;

};

#endif // BPHYSGENVALIDATION_H
