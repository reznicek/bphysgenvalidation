# BPhysGenValidation

Athena algorithm to read and check MC production (search for b-hadron decays and store their properties into a ntuple). The code is based on standard R22 analysis tools, using [`TRUTH0`](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TruthDAOD) derivation, which is to be produced via command `Reco_tf.py --inputEVNTFile=evgen.pool.root --outputDAODFile=DAOD_TRUTH0.root --reductionConf TRUTH0` in `Athena,23.0.4`. This is the recommended way at the [BLS MC-request page](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BPhysicsMCRequests).

## Compilation

Recommended directory structure:
```
build
src/BPhysGenValidation    # this GIT repository
run
```

The GIT repository `BPhysGenValidation` should be cloned into to `src` subdirectory (as indicated above):
```
cd src
git clone https://:@gitlab.cern.ch:8443/reznicek/BPhysGenValidation
```

Setup Athena environment (best in the top directory where `build`/`src`/`run` are placed):
```
setupATLAS
asetup Athena,22.0,latest
```

Copy and rename the top CMake file: `src/BPhysGenValidation/doc/top_CMakeLists.txt` -> `src/CMakeLists.txt`.
```
cd src
cp BPhysGenValidation/doc/top_CMakeLists.txt CMakeLists.txt
```

Link the data files into the `run` directory:
```
cd run
ln -s ../src/BPhysGenValidation/test/*.pdl ../src/BPhysGenValidation/test/*.DEC .
```

Finaly compile the code and link the executables into the run directory:
```
cd build
cmake ../src
make
. x86_64-centos7-gcc11-opt/setup.sh
cd ../run
ln -s ../build/x86_64-centos7-gcc11-opt/bin/* .
```

## Running the code

The example code [test/BPhysGenValidation_example.cxx](https://gitlab.cern.ch/reznicek/bphysgenvalidation/test/BPhysGenValidation_example.cxx) is to be run by:
```
cd run
./BPhysGenValidation_example --input DAOD_TRUTH0.root --output bdecays.root --nevents -1
```

## TODO

* Add code to read directly AthGeneration pool.root files.
* Make util/BPhysGenValidation as library, so that it is not compiled with each test-executable.

## Authors and acknowledgment

Main author: **Pavel Řezníček**

Contributing:
* Eliška Korcová (student's project at Charles University)

Based on / inspired by:
* [PhysicsAnalysis/DerivationFramework/DerivationFrameworkAnalysisTests](https://gitlab.cern.ch/atlas/athena/-/tree/21.2/PhysicsAnalysis/DerivationFramework/DerivationFrameworkAnalysisTests)
* [BLS trigger validation code](https://gitlab.cern.ch/atlas-trigger/BPhysicsLS/Analysis/bphystrigval_r22)
* [B->K*ee trigger validation](https://gitlab.cern.ch/atlas-trigger/BPhysicsLS/Analysis/BeeKTrigPackage/-/blob/master/python/makeTruthNtuple.py)

## License

Free to use and modify.
